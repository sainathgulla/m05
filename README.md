M05 - Installing Node.js

Getting ready to create web servers, services, and full stack apps with Node.js. 

Install Node.js (includes NPM) https://nodejs.org/en/ - A platform and runtime for building and running JavaScript applications. 

Open the Windows command prompt and type node -v. This should display the version. 

Type npm -v .  This should display your npm version (npm is a package manager that makes it really easy to install new capabilities). 
